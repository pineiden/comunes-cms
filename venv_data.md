#In postactivate file
=====================

[code]
#!/bin/bash
# This hook is sourced after this virtualenv is activated.
export DJANGO_SETTINGS_MODULE="comunes.settings.development"
export SECRET_KEY='mo1_8mu2&)sq6a$m#(yu$(h6#il7fsk_0ni+@0s5qu@+-50%3)'
export DATABASE_NAME=commons
export DATABASE_USER=dev
export DATABASE_PASSWORD=dev-locale
[/code]
