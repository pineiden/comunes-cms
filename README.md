# Sistema de Desarrollo

## Bash script tareas paralelas

 - revisión de script y documentación

## Documentación

 - documentación en Sphinx

## Gulp, sistema de gestión de tareas

 - Como y que instalar

## Livereload

 - Actualización browser

## Git

 - Gestión de versiones

# Comunes -CMS

## Diccionario temático

 - Conceptos
 - Relaciones a contenidos
 - Relaciones a Internet y documentos externos

## Articulos

 -Campos
 -Templates
 -Integración a redes sociales
 -Facebook Instant

## Sistema de usuarios

 -Tipos de usuarios
 -Registro de publicantes
 -Sistema de mensajería

## Tareas de publicación programadas

 - Bajo condiciones, publicar
 - Definición de tareas de revisión -publicación
 - Cambio de sistema de publicaciones

## Sistema de votación de artículos

 - Votación entre publicantes, categorías
 - Votación de lectores
 - Registro de votos y mensajería

## Registro visual de cada publicación periódica

 - Archivo histórico visual y de contenidos de pantallas
 - Torrents de archivo

## Impresión PDF

 - Registro PDF de contenidos y No de revistas
 - Disponibilidad de descarga con filtros temáticos

## Sistema de templates 

 - Esquema de conexiones de templates
 - Visualizaciones para home, contenidos, listas y formularios
 - Estilos CSS y Javascript
 - Frameworks CSS y JS 

## Formularios de contacto

 - Entre usuarios
 - Al sistema editorial
 - Desde lectores a publicantes
 - Correcciones a Diccionario [fe de erratas]

## Gestión de comentarios

 - Aprobación  - cada autor regula comentarios  - registro de evaluación disponible a circulo editor
 - Denuncia de comentarios no publicables

## Redes de Apoyo

 - Icono url, y contáctos
 - Espacios de financiamiento

## Discusión Editorial

 - Periodos constituyentes
 - Registro de actas

## Diseño

 - Criterios de diseño
 - Simbologías
 - Paletas de colores
 - Colaboradores


REF MARKDOWN: https://blue.cse.buffalo.edu/gitlab/help/markdown/markdown.md#headers
