.. Comunes-CMS documentation master file, created by
   sphinx-quickstart on Fri Jul  1 01:03:29 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Comunes-CMS's documentation!
=======================================

Contents:

.. toctree::
   :maxdepth: 2

	dev_sys


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

